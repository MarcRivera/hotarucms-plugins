BookmarkingPlugins
==================

Quickstart pack of Social Bookmarking Plugins for HotaruCMS

These packs are specifically for Hotaru users. Please download the files in the download link. This is the latest version. There are lots of improvement.

Social Bookmarking Plugin Pack
Download: <a href="https://www.wepaythemost.co.uk">bookmarking-pack.zip</a>
Last Updated: 2015/06/19

Contents: 
bookmarking,
categories,
category_manager,
comment_manager,
comments,
cron,
gravatar,
post_manager,
post_rss,
posts_widget,
recaptcha,
search,
stop_spam,
submit,
tags,
user_manager,
user_signin,
users,
vote,
widgets

More plugin packs for different kinds of websites will become available when ready.